#!/bin/bash

# Set the maximum number of retries
max_retries=4

# Set the sleep time between retries (in seconds)
sleep_time=10

# Loop until the command succeeds or the maximum number of retries is reached
for ((i=1; i<=max_retries; i++)); do
  echo "Attempt $i: kubectl apply -f clusters"
  
  # Run the kubectl apply command
  if kubectl apply -f clusters; then
    echo "Command succeeded on attempt $i"
    break
  else
    echo "Command failed on attempt $i"
    
    # Sleep for the specified number of seconds before retrying
    sleep "$sleep_time"
  fi
done
