#!/bin/bash

# Get the directory name from the first command-line argument
dir="$1"

# Loop over all YAML files in the directory
for file in "$dir"/*.yaml; do
  echo "Processing file: $file"
  
  # Extract the namespace from the input file using yq
  namespace=$(yq r "$file" metadata.namespace)
  
  if [ -z "$namespace" ]; then
    echo "Skipping file: $file (no namespace found)"
    continue
  fi
  
  echo "Namespace: $namespace"

  # Check if the namespace exists using kubectl
  if ! kubectl get namespace "$namespace" &> /dev/null; then
    # Create the namespace using kubectl
    kubectl create ns "$namespace"
    echo "Namespace created: $namespace"
    break  # Break out of the loop after the first namespace is created
  else
    echo "Namespace already exists: $namespace"
  fi
done
